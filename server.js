if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const path = require("path");
const express = require('express')
const app = express()
const bcrypt = require('bcrypt')
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
const methodOverride = require('method-override')
const axios = require("axios")
const mysql = require("mysql");
app.use(express.static(path.join(__dirname, "public")));
const initializePassport = require('./passport-config')
initializePassport(
  passport,
  email => users.find(user => user.email === email),
  id => users.find(user => user.id === id)
)


// database connection
const db = mysql.createConnection({
  host: "localhost",

  user: "root",

  password: "",

  database: "choose",
});
db.connect(function (err) {
  if (err) throw err;
  console.log("Connecté à la base de données MySQL!");
});
// users is used to verify the users and the password
const users = [];
// query database to retrieve all users
db.query("SELECT * FROM users", function (err, result) {
  if (err) throw err;
  result.map((element) => {
    verification(element);
    return users;
  });
});
// function which checks the users email address as well as his password
function verification(rows) {
  users.push(rows);
  initializePassport(
    passport,
    (email) => users.find((user) => user.email === email),
    (id) => users.find((user) => user.id === id)
  );
}

app.set('view-engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(flash())
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))



app.get('/',  (req, res) => {
  res.render('index.ejs')
})

app.get('/login', checkNotAuthenticated, (req, res) => {
  res.render('login.ejs')
})

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
  successRedirect: '/jeux',
  failureRedirect: '/login',
  failureFlash: true
}))

app.get('/register', checkNotAuthenticated, (req, res) => {
  res.render('register.ejs')
})

app.post("/register", checkNotAuthenticated, async (req, res) => {
  try {
    // use bcrypt to hash password
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    // mysql request to insert user in bdd
    let sql = "INSERT INTO users (Name, Email, Password) VALUES ?";
    // get name, email and password from our form
    let values = [[req.body.name, req.body.email, hashedPassword]];
    db.query(sql, [values], function (err, result) {
      if (err) throw err;
      console.log("Number of records inserted: " + result.affectedRows);
    });
    // if succes redirect to "/login"
    res.redirect("/login");
  } catch {
    // if error redirect "/register"
    res.redirect("/register");
  }
});
let games = []


db.query("SELECT * FROM games", function (err, result) {
  if (err) throw err;
  
  result.map((element) => {
    verification(element);
    games.push({name: element.name}, {rank: element.rank}, {image: element.image});
  });
  console.log(games)
});

app.get('/jeux', checkAuthenticated, (req, res) => {
  res.render('jeux.ejs', {name: req.user.name})
})

app.get('/apex', checkAuthenticated, (req, res) => {
  
  res.render('jeux/apex.ejs', {name: req.user.name})
})

app.delete('/logout', (req, res) => {
  req.logOut()
  res.redirect('/login')
})

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/login')
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/')
  }
  next()
}

app.listen(9090)